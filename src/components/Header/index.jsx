import React from 'react';
import {useHistory} from 'react-router-dom';
// @material-ui/core components
import {IconButton} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
import {grey} from '@material-ui/core/colors';
// @material-ui/icons components
import {VideoLibrary} from '@material-ui/icons';

const useStyles = makeStyles((theme) => ({
  header: {
    backgroundColor: theme.palette.primary.dark,
    height: '60px',
    display: 'flex',
    color: grey[100]
  },
  titleWrapper: {
    width: 'calc(100% - 60px)',
    display: 'flex',
    alignItems: 'center',
    paddingLeft: '20px',
    fontSize: '25px',
  },
  title: {
    cursor: 'pointer',
  },
  button: {
    display: 'flex',
    width: '60px',
    alignItems: 'center',
    justifyContent: 'center',
    borderLeft: '1px solid' + grey[100],
  },
  iconButton: {
    color: 'inherit'
  }
}));

const Header = () => {
  const classes = useStyles();
  const history = useHistory();

  return (
    <div className={classes.header}>
      <div className={classes.titleWrapper}>
        <span className={classes.title} onClick={() => history.push('/')}>Simplified YouTube</span>
      </div>
      <div className={classes.button}>
        <IconButton className={classes.iconButton} onClick={() => history.push('/upload')}>
          <VideoLibrary color='inherit'/>
        </IconButton>
      </div>
    </div>
  );
};

export default Header;
