import React from 'react';

const NoMatch = () => (
  <div>Oops! There is no such directory. Check your path! </div>
);

export default NoMatch;
