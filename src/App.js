import React from 'react';
import {Switch, Route} from 'react-router-dom';
// @material-ui/core components
import {makeStyles, createMuiTheme, ThemeProvider} from '@material-ui/core/styles';
import {red, teal} from '@material-ui/core/colors';
// core components
import MainPage from './views/MainPage';
import UploadVideoPage from './views/UploadVideoPage';
import Header from './components/Header';
import NoMatch from './components/NoMatch';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: teal[300],
      dark: teal[600],
    },
    error: {
      main: red[400]
    },
  },
});

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  }
}));

const App = () => {
  const classes = useStyles();

  return (
    <ThemeProvider theme={theme}>
      <Header/>
      <div className={classes.root}>
        <Switch>
          <Route exact path='/'>
            <MainPage/>
          </Route>
          <Route path='/upload'>
            <UploadVideoPage/>
          </Route>
          <Route path='*'>
            <NoMatch />
          </Route>
        </Switch>
      </div>
    </ThemeProvider>
  );
};

export default App;
