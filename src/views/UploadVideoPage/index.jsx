import React, {useState} from 'react';
import {ToastContainer, toast} from 'react-toastify';
// @material-ui/core components
import {makeStyles} from '@material-ui/core/styles';
import {Container, Typography, TextField, IconButton, Button} from '@material-ui/core';
import {grey} from '@material-ui/core/colors';
// @material-ui/icons components
import {AddAPhoto} from '@material-ui/icons';
// core components
import API, {API_URLS} from '../../api/config';
// styles
import 'react-toastify/dist/ReactToastify.css';

const useStyles = makeStyles((theme) => ({
  paper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  input: {
    display: 'none',
  },
  iconButton: {
    fontSize: '35px'
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    fontWeight: 600,
    color: grey[100]
  },
  error: {
    color: theme.palette.error.main,
  }
}));

const UploadVideoPage = () => {
  const classes = useStyles();

  const [title, setTitle] = useState('');
  const [error, setError] = useState(false);
  const [video, setVideo] = useState('');

  const onChangeVideo = event => {
    error && setError(false);
    if (event.target.files[0]) {
      setVideo(event.target.files[0]);
    }
  };

  const onChangeTitle = event => {
    setTitle(event.target.value);
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    if (video) {
      const newData = new FormData();
      newData.append('video', video);
      newData.append('title', title);
      try {
        const response = await API.post(API_URLS.videos, newData);
        if (response.status === 200) {
          toast.success('Upload successful');
          setTitle('');
          setVideo('');
        }
      } catch (e) {
        toast.error('Upload failed');
      }
    } else {
      setError(true);
    }
  };

  return (
    <Container component='main' maxWidth='xs'>
      <Typography component='h1' variant='h5'>
        Upload your video
      </Typography>
      <form onSubmit={onSubmit} encType='multipart/form-data'>
        <TextField
          value={title}
          variant='standard'
          margin='normal'
          required
          fullWidth
          id='title'
          label='Video title'
          name='title'
          autoComplete='title'
          onChange={onChangeTitle}
        />
        <input
          accept='video/*'
          className={classes.input}
          id='video-file'
          name='video'
          type='file'
          onChange={onChangeVideo}
        />
        <label htmlFor='video-file'>
          <IconButton
            color='primary'
            aria-label='upload picture'
            component='span'
            className={classes.iconButton}
          >
            <AddAPhoto fontSize='inherit'/>
          </IconButton>
        </label>
        <span>
          {error ? <span className={classes.error}>Please, upload video</span> : video.name}
        </span>
        <Button
          type='submit'
          fullWidth
          variant='contained'
          color='primary'
          className={classes.submit}
        >
          Upload
        </Button>
      </form>
      <div className='form-group'>
        <ToastContainer position='bottom-right'/>
      </div>
    </Container>
  );
};

export default UploadVideoPage;
