import React, {useState, useEffect} from 'react';
import {ToastContainer, toast} from 'react-toastify';
// @material-ui/core components
import {Grid, CircularProgress, Typography} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
// core components
import Video from './components/Video';
import API, {API_URLS} from '../../api/config';
// styles
import 'react-toastify/dist/ReactToastify.css';

const useStyles = makeStyles((theme) => ({
  title: {
    fontSize: '25px',
  },
  videoList: {
    marginTop: theme.spacing(1),
  },
  noVideosWrapper: {
    marginLeft: theme.spacing(1),
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    height: '200px',
  }
}));

const MainPage = () => {
  const classes = useStyles();
  const [videos, setVideos] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      try {
        setIsLoading(true);
        const response = await API.get(API_URLS.videos);
        setVideos(response.data);
        setIsLoading(false);
      } catch (e) {
        toast.error("Server error: can't receive videos");
      }
    };
    fetchData();
  }, []);

  const content = videos && videos.length > 0
    ? videos.map((video) => <Video key={video.id} id={video.id} title={video.title}/>)
    : <div className={classes.noVideosWrapper}>No videos yet</div>;

  return (
    <div>
      <Typography component="h1" variant="h5">
        Videos
      </Typography>
      <Grid container spacing={3} className={classes.videoList}>
        {isLoading ? <div className={classes.noVideosWrapper}><CircularProgress/></div> : content}
      </Grid>
      <div className='form-group'>
        <ToastContainer position='bottom-right'/>
      </div>
    </div>
  );
};

export default MainPage;
