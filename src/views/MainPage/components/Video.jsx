import React from 'react';
import {Player} from 'video-react';
import PropTypes from 'prop-types';
// @material-ui/core components
import {Grid} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
// core components
import {BASE_URL} from '../../../api/config';
// styles
import 'video-react/dist/video-react.css';

const useStyles = makeStyles((theme) => ({
  videoWrapper: {},
  title: {
    fontSize: '16px',
    padding: theme.spacing(1),
    height: '30px',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
  }
}));

const Video = ({id, title}) => {
  const classes = useStyles();

  const URL = `${BASE_URL}/uploads/${id}`;

  return (
    <Grid item xs={12} sm={6} md={4} lg={3} className={classes.root}>
      <div className={classes.videoWrapper}>
        <Player>
          <source src={URL}/>
        </Player>
      </div>
      <div className={classes.title}>
        {title}
      </div>
    </Grid>
  );
};

Video.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
};

export default Video;
